function show_menu(){
    $(".mobile-menu").animate({left: '0'});
    $(".mobile-menu-overlay").show();
}
function close_menu(){
    $(".mobile-menu").animate({left:'-250px'});
    $(".mobile-menu-overlay").hide();
}


$("document").ready(function(){
    $(".hamburger-menu").click(function(){
       show_menu();
    });

    $(".menu-close").click(function(){
       close_menu();
    });

    $(".mobile-menu-overlay").on('click', function(){
        close_menu();
    });
});
